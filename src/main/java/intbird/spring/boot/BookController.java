package intbird.spring.boot;

import com.google.gson.Gson;
import intbird.spring.boot.mysql.Book;
import intbird.spring.boot.mysql.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * created by intbird
 * on 2019/07/08
 */
@RestController
@RequestMapping("/book")
public class BookController {

    @Autowired
    private IBookService bookService;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public List<Book> getAllBook() {
        return bookService.getAllBook();
    }

    @RequestMapping(value = "search", method = RequestMethod.GET)
    public Book searchBook(Long id) {
        return bookService.searchBook(id);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Book insertBook(@ModelAttribute Book book) {
        return bookService.insertBook(book);
    }

    @RequestMapping(value = "/update/{id}/{ids}", method = RequestMethod.POST)
    public Book updateBook(@PathVariable Long id, @PathVariable Long ids, @ModelAttribute Book book) {
        System.out.println("同名被替换:" + new Gson().toJson(book));
        return bookService.updateBook(book);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public void deleteBook(@PathVariable Long id) {
        bookService.deleteBook(id);
    }

}
