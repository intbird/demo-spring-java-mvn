package intbird.spring.boot.mysql;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * created by intbird
 * on 2019/07/08
 */
@Repository
public class BookDaoImpl implements IBookDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Book> getAllBook() {
        String sql = "SELECT id, name, writer FROM t_book";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Book.class));
    }

    @Override
    public Book searchBook(Long id) {
        String sql = "SELECT id, name, writer FROM t_book WHERE id=?";
        return (Book) jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper(Book.class));
    }

    @Override
    public Book insertBook(Book book) {
        String sql = "INSERT INTO t_book(id,name,writer) VALUES(?,?,?)";
        jdbcTemplate.update(sql, book.getId(), book.getName(), book.getWriter());
        return book;
    }

    @Override
    public Book updateBook(Book book) {
        String sql = "UPDATE t_book SET name=?, writer = ? WHERE id=?";
        jdbcTemplate.update(sql, book.getName(), book.getWriter(), book.getId());
        return book;
    }

    @Override
    public void deleteBook(Long id) {
        String sql = "DELETE FROM t_book WHERE id=?";
        jdbcTemplate.update(sql, id);
    }
}
