package intbird.spring.boot.mysql;

import java.util.List;

/**
 * created by intbird
 * on 2019/07/08
 */
public interface IBookDao {

    List<Book> getAllBook();

    Book searchBook(Long id);

    Book insertBook(Book book);

    Book updateBook(Book book);

    void deleteBook(Long id);

}
