package intbird.spring.boot.mysql;

import java.io.Serializable;

/**
 * created by intbird
 * on 2019/07/08
 */
public class Book implements Serializable {

    private Long id;
    private String name;
    private String writer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }
}
