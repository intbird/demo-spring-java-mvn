package intbird.spring.boot.mysql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * created by intbird
 * on 2019/07/08
 */
@Service
public class BookServiceImpl implements IBookService {

    @Autowired
    private IBookDao bookDao;

    @Override
    public List<Book> getAllBook() {
        return bookDao.getAllBook();
    }

    @Override
    public Book searchBook(Long id) {
        return bookDao.searchBook(id);
    }

    @Override
    public Book insertBook(Book book) {
        return bookDao.insertBook(book);
    }

    @Override
    public Book updateBook(Book book) {
        return bookDao.updateBook(book);
    }

    @Override
    public void deleteBook(Long id) {
        bookDao.deleteBook(id);
    }
}
