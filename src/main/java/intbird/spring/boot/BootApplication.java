package intbird.spring.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * created by intbird
 * on 2019/07/08
 */
@RestController
@SpringBootApplication
public class BootApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootApplication.class, args);
    }

    @ModelAttribute
    public void attributeModel(String id, String name, Model model) {
        model.addAttribute("id", id);
        model.addAttribute("name", name);
    }

    @RequestMapping(value = "/hello")
    public String helloWord(String params) {
        return "hello word " + params;
    }
}
